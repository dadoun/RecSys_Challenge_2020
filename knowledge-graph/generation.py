"""
Generate the User graph
"""
import argparse
import os
from os import path

from tqdm import tqdm

from dataset import Dataset

account_classes = [
    (0, 150),
    (1, 500),
    (2, 1000),
    (3, 10000),
    (4, 100000),
    (5, 1000000),
    (6, 10000000),
    (7, 200000000),
]

action_labels = ['reply_timestamp', 'retweet_timestamp', 'retweet_with_comment_timestamp', 'like_timestamp']
edg_names = ['user_class', 'follows', 'no_action', 'like', 'retweet', 'retweet_with_comment', 'reply', 'author',
             'language', 'tweet_type', 'domain', 'media', 'hashtag']


def to_action(label):
    return label.replace('_timestamp', '')


def EDGE(a, b):
    if not a or not b:
        return ''
    return '%s\t%s\n' % (str(a), str(b))


def assign_account_class(n_followers):
    for i, threshold in account_classes:
        if n_followers <= threshold:
            return str(i)
    return str(account_classes[-1][0])  # default: the max class


def run(input_path, output_dir='./edgelist', no_action=False, mask_file=None):
    # prepare edgelist files to write
    os.makedirs(output_dir, exist_ok=True)
    edg = {}
    for n in edg_names:
        edg[n] = open(path.join(output_dir, '%s.edgelist' % n), 'w')

    mask = False
    if mask_file:
        with open(mask_file) as f:
            mask = [x == 'True\n' for x in f.readlines()]

    # parse line by line
    dataset = Dataset(input_path)
    reader = dataset.as_dict_reader()
    for i, line in enumerate(tqdm(reader)):
        if mask and not mask[i]:
            continue

        u1 = line['engaging_user_id']  # reader
        u1_followers = int(line['engaging_user_following_count'])
        u2 = line['engaged_with_user_id']  # author
        u2_followers = int(line['engaged_with_user_follower_count'])
        tweet = line['tweet_id']

        if line['engagee_follows_engager']:
            edg['follows'].write(EDGE(u1, u2))

        edg['user_class'].write(EDGE(u1, "CLASS" + assign_account_class(u1_followers)))
        edg['user_class'].write(EDGE(u2, "CLASS" + assign_account_class(u2_followers)))

        edg['author'].write(EDGE(u2, tweet))

        if not no_action:
            actions = [edg[to_action(a)].write(EDGE(u1, tweet))
                       for a in action_labels if line[a]]
            if len(actions) == 0:
                edg['no_action'].write(EDGE(u1, tweet))

        edg['language'].write(EDGE(tweet, line['language']))
        edg['tweet_type'].write(EDGE(tweet, line['tweet_type']))

        for domain in line['present_domains'].split('\t'):
            edg['domain'].write(EDGE(tweet, domain))

        for hashtag in line['hashtags'].split('\t'):
            edg['hashtag'].write(EDGE(tweet, hashtag))

        if line['present_media']:
            media = line['present_media'].split('\t')
            if len(media) > 1:
                media = ['Photos']
            edg['media'].write(EDGE(tweet, media[0]))

    # close files
    dataset.close()
    for n in edg_names:
        edg[n].close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Generate the User graph.')
    parser.add_argument('-i', '--input', help='input csv',
                        default='../Data/Training_2_M_Data.tsv')
    parser.add_argument('-o', '--output', help='output edgelist folder',
                        default='./edgelist')
    parser.add_argument('--no_action', help='exclude actions from the edgelists',
                        action='store_true', default=False)
    parser.add_argument('--mask', help='Optional mask file')
    args = parser.parse_args()

    run(args.input, args.output, args.no_action, args.mask)

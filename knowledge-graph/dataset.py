import csv
import pandas as pd

FIELD_NAMES = ["text_ tokens", "hashtags", "tweet_id", "present_media", "present_links", "present_domains",
               "tweet_type", "language", "tweet_timestamp", "engaged_with_user_id", "engaged_with_user_follower_count",
               "engaged_with_user_following_count", "engaged_with_user_is_verified",
               "engaged_with_user_account_creation", "engaging_user_id", "engaging_user_follower_count",
               "engaging_user_following_count", "engaging_user_is_verified", "engaging_user_account_creation",
               "engagee_follows_engager", "reply_timestamp", "retweet_timestamp",
               "retweet_with_comment_timestamp", "like_timestamp"]


class Dataset:
    def __init__(self, path):
        self.path = path

    def as_dict_reader(self):
        self.input_file = open(self.path, 'r', encoding='utf-8')
        csv.register_dialect('recsys20', delimiter='\x01', quoting=csv.QUOTE_NONE)
        return csv.DictReader(self.input_file, fieldnames=FIELD_NAMES, dialect='recsys20')

    def close(self):
        if self.input_file:
            self.input_file.close()

    def as_pandas(self, usecols=None):

        return pd.read_csv(self.path, sep='\x01', encoding='utf-8', usecols=usecols,
                           header=None, names=FIELD_NAMES)

Knowledge Graph Embeddings
==========================

Generate KG embeddings on the dataset.

### Install Dependencies

```
pip install -r requirements.txt
```

### Generate the graph

```
generation.py [-i INPUT] [-o OUTPUT] [--no_action] [--mask MASK]
```

Arguments:
 - `-i`, `--input` Input dataset tsv.
 - `-o`, `--output` Output edgelist folder.  Default: `./edgelist/`
 - `--no_action`    Exclude actions (like, reply, ...) from the edgelists
 - `--mask`           Optional mask file, containing one line per tsv row with a `True` or `False`
 
### Compute the embeddings

```
embed.py [-h] [-i INPUT] [-o OUTPUT] [-d DIMENSION] [-c CONFIG]
```

Arguments:
 - `-i`, `--input` Input edgelist folder. Default: `./edgelist/`
 - `-o`, `--output` Output embeddings file. Default: `./embeddings/general.bin`
 - `-d`, `--dimension` Number of output embedding dimensions. Default: `100`
 - `-c`, `--config`. Config file, for weighting each edgelists. Default: `config.yml` (to look as example)


import os
from os import path
import argparse
import yaml
import time
import networkx as nx
from tqdm import tqdm
from nodevectors import Node2Vec
from gensim.models.callbacks import CallbackAny2Vec


class Callback(CallbackAny2Vec):
    """Callback to print loss after each epoch."""

    def __init__(self, total_epochs):
        self.epoch = 0
        self.total_epochs = total_epochs

    def on_epoch_end(self, model):
        self.epoch += 1
        print('Epoch {}/{} completed'.format(self.epoch, self.total_epochs))


def run(input_dir, output='./embeddings/general.bin', dimensions=100, weights=None):
    if weights is None:
        weights = dict()

    os.makedirs(output.rsplit('/', maxsplit=1)[0], exist_ok=True)

    print('loading edgelists...')
    edgelists = [path.join(root, f) for root, dirs, files in os.walk(input_dir) for f in files
                 if f.endswith('.edgelist')]
    G = nx.Graph()
    for edgelist in tqdm(sorted(edgelists)):
        name = edgelist.rsplit('/', 1)[1].rsplit('.', 1)[0]
        weight = float(weights.get(name, 1))
        if weight == 0:  # ignore this edgelist
            continue

        with open(edgelist) as f:
            for line in f:
                a, b = line[0:-1].split('\t')
                G.add_edge(a, b, weight=weight)

    print('Nodes: %d' % nx.number_of_nodes(G))
    print('Edges: %d' % nx.number_of_edges(G))

    print('Start learning at %s' % time.asctime())
    g2v = Node2Vec(
        walklen=6,
        epochs=10,
        n_components=dimensions,
        w2vparams={
            'min_count': 10,
            'callbacks': [Callback(5)]
        })
    g2v.fit(G, verbose=True)
    print('End learning at %s' % time.asctime())

    # Save model to gensim.KeyedVector format
    g2v.save_vectors(output)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Generate the User graph.')
    parser.add_argument('-i', '--input', help='input edgelist folder',
                        default='./edgelist')
    parser.add_argument('-o', '--output', help='output embeddings file',
                        default='./embeddings/general.bin')
    parser.add_argument('-d', '--dimension', type=int, help='number of output embedding dimensions',
                        default=100)
    parser.add_argument('-c', '--config', help='config file',
                        default='config.yml')
    args = parser.parse_args()

    with open(args.config) as f:
        config = yaml.load(f, Loader=yaml.BaseLoader)

    run(args.input, args.output, args.dimension, config.get('weights', {}))

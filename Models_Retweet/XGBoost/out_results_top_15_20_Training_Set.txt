(21849557, 15)
(21849557,)
[14:44:42] INFO: /workspace/src/tree/updater_prune.cc:89: tree pruning end, 1488 extra nodes, 44 pruned nodes, max_depth=10
[0]	validation_0-aucpr:0.61889
Will train until validation_0-aucpr hasn't improved in 3 rounds.
[14:45:04] INFO: /workspace/src/tree/updater_prune.cc:89: tree pruning end, 1446 extra nodes, 82 pruned nodes, max_depth=10
[1]	validation_0-aucpr:0.65088
[14:45:26] INFO: /workspace/src/tree/updater_prune.cc:89: tree pruning end, 1562 extra nodes, 74 pruned nodes, max_depth=10
[2]	validation_0-aucpr:0.65696
[14:45:49] INFO: /workspace/src/tree/updater_prune.cc:89: tree pruning end, 1712 extra nodes, 74 pruned nodes, max_depth=10
[3]	validation_0-aucpr:0.65636
[14:46:13] INFO: /workspace/src/tree/updater_prune.cc:89: tree pruning end, 1576 extra nodes, 118 pruned nodes, max_depth=10
[4]	validation_0-aucpr:0.66558
[14:46:37] INFO: /workspace/src/tree/updater_prune.cc:89: tree pruning end, 1740 extra nodes, 82 pruned nodes, max_depth=10
[5]	validation_0-aucpr:0.66749
[14:47:01] INFO: /workspace/src/tree/updater_prune.cc:89: tree pruning end, 1610 extra nodes, 106 pruned nodes, max_depth=10
[6]	validation_0-aucpr:0.67044
[14:47:24] INFO: /workspace/src/tree/updater_prune.cc:89: tree pruning end, 1716 extra nodes, 78 pruned nodes, max_depth=10
[7]	validation_0-aucpr:0.67183
[14:47:49] INFO: /workspace/src/tree/updater_prune.cc:89: tree pruning end, 1572 extra nodes, 96 pruned nodes, max_depth=10
[8]	validation_0-aucpr:0.67321
[14:48:10] INFO: /workspace/src/tree/updater_prune.cc:89: tree pruning end, 1702 extra nodes, 88 pruned nodes, max_depth=10
[9]	validation_0-aucpr:0.67436
[14:48:31] INFO: /workspace/src/tree/updater_prune.cc:89: tree pruning end, 1622 extra nodes, 74 pruned nodes, max_depth=10
[10]	validation_0-aucpr:0.67531
[14:48:56] INFO: /workspace/src/tree/updater_prune.cc:89: tree pruning end, 1688 extra nodes, 92 pruned nodes, max_depth=10
[11]	validation_0-aucpr:0.67599
[14:49:20] INFO: /workspace/src/tree/updater_prune.cc:89: tree pruning end, 1800 extra nodes, 62 pruned nodes, max_depth=10
[12]	validation_0-aucpr:0.67747
[14:49:45] INFO: /workspace/src/tree/updater_prune.cc:89: tree pruning end, 1780 extra nodes, 106 pruned nodes, max_depth=10
[13]	validation_0-aucpr:0.67795
[14:50:09] INFO: /workspace/src/tree/updater_prune.cc:89: tree pruning end, 1670 extra nodes, 80 pruned nodes, max_depth=10
[14]	validation_0-aucpr:0.67829
**** Training results ****
PRAUC LIKE: PRAUC= 0.6807638076103097
RCE LIKE: RCE= 41.0522941945652
**** Test results ****
PRAUC LIKE: PRAUC= 0.6782929508234494
RCE LIKE: RCE= 40.84204530637018
[14:51:02] ======== Monitor: Learner ========
[14:51:02] Configure: 1e-06s, 1 calls @ 1us

[14:51:02] EvalOneIter: 14.4506s, 15 calls @ 14450650us

[14:51:02] GetGradient: 2.29742s, 15 calls @ 2297416us

[14:51:02] PredictRaw: 0.630851s, 15 calls @ 630851us

[14:51:02] UpdateOneIter: 313.485s, 15 calls @ 313485126us

[14:51:02] ======== Monitor: GBTree ========
[14:51:02] BoostNewTrees: 324.971s, 15 calls @ 324971495us

[14:51:02] CommitModel: 10.5846s, 15 calls @ 10584636us

[14:51:02] ======== Monitor:  ========

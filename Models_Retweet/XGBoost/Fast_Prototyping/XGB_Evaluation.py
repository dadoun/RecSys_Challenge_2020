import random
import numpy as np
import pandas as pd
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from matplotlib import pyplot as plt
import itertools
from sklearn.metrics import precision_score, recall_score, roc_auc_score, roc_curve, precision_recall_curve, average_precision_score
from sklearn.metrics import roc_auc_score, confusion_matrix
from sklearn.preprocessing import MinMaxScaler
from xgboost import XGBClassifier
from sklearn.model_selection import StratifiedKFold, KFold
from sklearn.model_selection import StratifiedKFold, RepeatedStratifiedKFold,cross_val_score
from sklearn.metrics import precision_recall_curve, auc, log_loss
import joblib

## Loading the data
X_train = np.load('/opt/recsys20_challenge/dataset/Data_Likes/XGBoost_Data/Fast_Proto/X_train.npy')
Y_train = np.load('/opt/recsys20_challenge/dataset/Data_Retweet/XGBoost_Data/Fast_Proto/Y_train.npy')
#####
X_test = np.load('/opt/recsys20_challenge/dataset/Data_Likes/XGBoost_Data/Fast_Proto/X_test.npy')
Y_test = np.load('/opt/recsys20_challenge/dataset/Data_Retweet/XGBoost_Data/Fast_Proto/Y_test.npy')

## Adding like predictions
#xgb_like_model = joblib.load('../../../Models_Like/XGBoost/Fast_Prototyping/xgb.dat')
#X_probs_train = xgb_like_model.predict(X_train)#[:, 1]
#X_probs_test = xgb_like_model.predict(X_test)#[:, 1]
#X_train = np.concatenate([X_train, np.reshape(X_probs_train, (-1,1))], axis=1)
#X_test = np.concatenate([X_test, np.reshape(X_probs_test, (-1,1))], axis=1)


## Defining the model
xgb_model = XGBClassifier(objective="binary:logistic", max_depth=8, n_estimators=15, gamma=1, colsample_bytree=0.8,
subsample=0.8, random_state=42, verbosity=3, tree_method='gpu_hist')#, scale_pos_weight=(len(Y_train)-sum(Y_train))/sum(Y_train), verbosity=3)

## Fitting tr data
xgb_model.fit(X_train, Y_train, early_stopping_rounds=3, eval_metric="logloss", eval_set=[(X_test, Y_test)], verbose =True)

joblib.dump(xgb_model, "xgb.dat")

#### Training predictions (to demonstrate overfitting in case)
train_xgb_predictions = xgb_model.predict(X_train)
train_xgb_probs = xgb_model.predict_proba(X_train)[:, 1]
# Actual class predictions
xgb_predictions = xgb_model.predict(X_test)
# Probabilities for each class
xgb_probs = xgb_model.predict_proba(X_test)[:, 1]


## Evaluation Metrics
def compute_prauc(pred, gt):
    prec, recall, thresh = precision_recall_curve(gt, pred)
    prauc = auc(recall, prec)
    return prauc

def calculate_ctr(gt):
    positive = len([x for x in gt if x == 1])
    ctr = positive/float(len(gt))
    return ctr

def compute_rce(pred, gt):
    cross_entropy = log_loss(gt, pred)
    data_ctr = calculate_ctr(gt)
    strawman_cross_entropy = log_loss(gt, [data_ctr for _ in range(len(gt))])
    return (1.0 - cross_entropy/strawman_cross_entropy)*100.0 


print('********** Train Results ***************')
## PR AUC & RCE Metric
prauc = compute_prauc(train_xgb_probs, Y_train)
print('PRAUC LIKE: PRAUC=', prauc)
rce = compute_rce(train_xgb_probs, Y_train)
print('RCE LIKE: RCE=', rce)

print('********** Test Results ***************')
## PR AUC & RCE Metric
prauc = compute_prauc(xgb_probs, Y_test)
print('PRAUC LIKE: PRAUC=', prauc)
rce = compute_rce(xgb_probs, Y_test)
print('RCE LIKE: RCE=', rce)

##
l = ['present_domains', 'tweet_type', 'language',
       'present_media', 'engagee_follows_engager', 'hashtags',
       'engaging_user_follower_count', 'engaging_user_following_count',
       'engaged_with_user_follower_count', 'engaged_with_user_following_count',
       'users_int', 'authors_int',
       'nb_likes', 'nb_reply', 'nb_retweet', 'nb_retweet_C',   
       'nb_likes_author', 'nb_reply_author', 'nb_retweet_author', 'nb_retweet_C_author',
       'engaging_user_account_creation', 'engaged_with_user_account_creation',
       'engaged_with_user_is_verified', 'engaging_user_is_verified']
#for i in range(768):
#    l.append('l_'+str(i))
# Extract feature importances
fi = pd.DataFrame({'feature': l,
                   'importance': xgb_model.feature_importances_}).\
                    sort_values('importance', ascending = False)

# Display
print(fi.values)

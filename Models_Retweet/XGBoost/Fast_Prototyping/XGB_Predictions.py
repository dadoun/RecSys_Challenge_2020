import joblib
import numpy as np
import pandas as pd

X_eval = np.load('/opt/recsys20_challenge/dataset/Data_Likes/XGBoost_Data/X_eval.npy')
#
xgb_model = joblib.load("xgb.dat")
#
eval_predictions = xgb_model.predict_proba(X_eval)[:, 1]
#
df_eval = pd.read_csv('/opt/recsys20_challenge/dataset/Data_Likes/XGBoost_Data/uids_tids_p.csv')
#
df_eval['Prediction'] = list(eval_predictions)
#
df_eval.rename(columns={'tweet_id':'Tweet_Id',
                          'engaging_user_id':'User_Id'}, 
                 inplace=True)
df_eval = df_eval[['Tweet_Id', 'User_Id', 'Prediction']]
#
df_eval.to_csv('Likes_Predictions.csv', index=None, header=None)
# D2KLab @RecSysChallenge2020

This repository contains source codes and experiments by the [D2KLab](https://github.com/D2KLab) in the context of the RecSys Challenge 2020.

All the code relies on Python (latest version).

## Install requirements

    pip install -r requirements.txt

The rest of the documentation refers to the `submission` folder.

## Method A: XGBoost

Folder `XGBoost`

- Modify `config.py` in order to fit your folder preferences
- Execute the preprocessing scripts
    ```
    python preprocessing_x_train_part1.py
    python preprocessing_x_eval.py competition_test`
    python preprocessing_y <what>
  ```
   where what is in succession 'like', 'reply', 'retweet', 'retweet_with_comment'.
    - Example `python preprocessing_y like`
- Generate XGB model with `python train.py <what>`
- Execute the Notebook `TFIDF prediction`
    - after editing the first 2 cells with folders and interaction to be extracted
    - process it one time per each interaction
- Use the model for prediction with `python predict.py <what>`

## Method B: XGBoost + TF-IDF Features

- Execute Method A
- Execute the Notebook `Models_Ensembling`
    - after editing the first 1 cells with folders

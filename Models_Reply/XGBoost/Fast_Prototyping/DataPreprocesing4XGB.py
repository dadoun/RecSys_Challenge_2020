import pandas as pd
import numpy as np
import random
import os
from sklearn.preprocessing import MinMaxScaler

## Read training data (Will be splited to tr/test data for local validation & parameters tuning)

print('**************************** READING THE DATA ******************************************')


df_40_M = pd.read_csv('/opt/recsys20_challenge/dataset/Training_2_M_Data.tsv', sep='\x01', encoding='utf-8', header=None,
                      names=["text_ tokens", "hashtags", "tweet_id", "present_media", "present_links", "present_domains",\
                "tweet_type","language", "tweet_timestamp", "engaged_with_user_id", "engaged_with_user_follower_count",\
               "engaged_with_user_following_count", "engaged_with_user_is_verified", "engaged_with_user_account_creation",\
               "engaging_user_id", "engaging_user_follower_count", "engaging_user_following_count", "engaging_user_is_verified",\
               "engaging_user_account_creation", "engagee_follows_engager", "reply_timestamp", "retweet_timestamp", 
                      "retweet_with_comment_timestamp", "like_timestamp"], usecols = ['retweet_timestamp'])

df_40_M.reply_timestamp.fillna('NAN', inplace=True)
## Create the target column
## Fill timestamp column
mtx_df = df_40_M['reply_timestamp'].values
list_labels=[]
for i in range(len(mtx_df)):
    if not(mtx_df[i] == 'NAN'):
        list_labels.append(1)
    else:
        list_labels.append(0)
df_40_M['Y']=list_labels
##
df_40_M.drop('reply_timestamp', axis=1, inplace=True)
##
print('NB Y=1: ', np.sum(df_40_M['Y'].values))
print('Len DF: ', len(df_40_M))
##
print('Fill Null Values?')
##
# Split training/test data
test_len = int(0.1*len(df_40_M))
df_test = df_40_M.iloc[0:test_len,:]
print('test OK')
df_train = df_40_M.iloc[test_len:len(df_40_M),:]
print('train OK')

Y_train = df_train['Y']
Y_test = df_test['Y']
##
##
## Save all data (without split)
np.save('/opt/recsys20_challenge/dataset/Data_Reply/XGBoost_Data/Fast_Proto/Y_train.npy', Y_train)
#####
np.save('/opt/recsys20_challenge/dataset/Data_Reply/XGBoost_Data/Fast_Proto/Y_test.npy', Y_test)

'''
print('**************************** READING THE DATA ******************************************')


df_40_M = pd.read_csv('/opt/recsys20_challenge/dataset/Training_2_M_Data.tsv', sep='\x01', encoding='utf-8', header=None,
                      names=["text_ tokens", "hashtags", "tweet_id", "present_media", "present_links", "present_domains",\
                "tweet_type","language", "tweet_timestamp", "engaged_with_user_id", "engaged_with_user_follower_count",\
               "engaged_with_user_following_count", "engaged_with_user_is_verified", "engaged_with_user_account_creation",\
               "engaging_user_id", "engaging_user_follower_count", "engaging_user_following_count", "engaging_user_is_verified",\
               "engaging_user_account_creation", "engagee_follows_engager", "reply_timestamp", "retweet_timestamp", 
                      "retweet_with_comment_timestamp", "like_timestamp"], usecols = ['engaging_user_id', 'tweet_id', 'present_domains', 'tweet_type', 'language',
       'present_media', 'engagee_follows_engager', 'hashtags',
       'engaging_user_follower_count', 'engaging_user_following_count',
       'engaged_with_user_follower_count', 'engaged_with_user_following_count', 
       'engaged_with_user_is_verified', 'engaging_user_is_verified',
       'engaging_user_account_creation', 'engaged_with_user_account_creation',
       'reply_timestamp', 'retweet_timestamp', 'retweet_with_comment_timestamp',
       'like_timestamp'])

print('**************************** ADDING (user,tweet_int) columns ******************************************')
## Add two columns users_int, items_int
df_users_id = df_40_M.engaging_user_id.value_counts()
dict_users_nb_int = dict(zip(list(df_users_id.index), list(df_users_id)))
### For users
list_nb_int_users = list()
mtx = df_40_M['engaging_user_id'].values
for i in range(len(df_40_M)):
    list_nb_int_users.append(dict_users_nb_int[mtx[i]])
df_40_M['users_int'] = list_nb_int_users
### For Tweets
df_tweets_id = df_40_M.tweet_id.value_counts()
dict_tweets_nb_int = dict(zip(list(df_tweets_id.index), list(df_tweets_id)))
###
list_nb_int_tweets = list()
mtx = df_40_M['tweet_id'].values
for i in range(len(df_40_M)):
    list_nb_int_tweets.append(dict_tweets_nb_int[mtx[i]])
df_40_M['tweets_int'] = list_nb_int_tweets

def boolean_replacement(df, column, new_column):
    df[column].fillna('NAN', inplace=True)
    ## Create the target column
    ## Fill timestamp column
    mtx_df = df[column].values
    list_labels=[]
    for i in range(len(mtx_df)):
        if not(mtx_df[i] == 'NAN'):
            list_labels.append(1)
        else:
            list_labels.append(0)
    df[new_column]=list_labels
    ##
    df.drop(column, axis=1, inplace=True)
    return df
##
def nb_eng(engagement='likes', column='Y', df=df_40_M):
    df_Y_1 = df[df[column]==1].engaging_user_id.value_counts() -1 
    dict_nb_likes = dict(zip(list(df_Y_1.index), list(df_Y_1)))
    ### For users
    list_nb_likes = list()
    mtx = df['engaging_user_id'].values
    for i in range(len(df)):
        if mtx[i] in dict_nb_likes:
            list_nb_likes.append(dict_nb_likes[mtx[i]])
        else:
            list_nb_likes.append(0)
    df['nb_' + engagement] = list_nb_likes
    return df    
##
def nb_eng_tweet(engagement='likes', column='Y', df=df_40_M):
    df_Y_1 = df[df[column]==1].tweet_id.value_counts() -1 
    dict_nb_likes = dict(zip(list(df_Y_1.index), list(df_Y_1)))
    ### For users
    list_nb_likes = list()
    mtx = df['tweet_id'].values
    for i in range(len(df)):
        if mtx[i] in dict_nb_likes:
            list_nb_likes.append(dict_nb_likes[mtx[i]])
        else:
            list_nb_likes.append(0)
    df['nb_' + engagement + '_tweet'] = list_nb_likes
    return df


print('**************** Boolean Replacement *******************')
df_40_M = boolean_replacement(df_40_M, 'like_timestamp', 'likes_Y')
df_40_M = boolean_replacement(df_40_M, 'reply_timestamp', 'Y')
df_40_M = boolean_replacement(df_40_M, 'retweet_timestamp', 'retweet_Y')
df_40_M = boolean_replacement(df_40_M, 'retweet_with_comment_timestamp', 'retweet_C_Y')
##########
print('**************** Interactions per Engagement *******************')
# Compute nb_interaction per type of interaction for users
df_40_M = nb_eng('likes', 'likes_Y')
df_40_M = nb_eng('reply', 'Y')
df_40_M = nb_eng('retweet', 'retweet_Y')
df_40_M = nb_eng('retweet_C', 'retweet_C_Y')
# Compute nb_interaction per type of interaction for tweets
df_40_M = nb_eng_tweet('likes', 'likes_Y')
df_40_M = nb_eng_tweet('reply', 'Y')
df_40_M = nb_eng_tweet('retweet', 'retweet_Y')
df_40_M = nb_eng_tweet('retweet_C', 'retweet_C_Y')

## What is the size of our dataset?
print('NB Y=1: ', np.sum(df_40_M['Y'].values))
print('Len DF: ', len(df_40_M))
##
print('Fill Null Values')
##
df_40_M.fillna('NAN', inplace=True)
print('Change Type of some columns')
### change types of some columns
df_40_M = df_40_M.astype({'engaging_user_is_verified': 'int8', 'engaged_with_user_is_verified': 'int8', 'engagee_follows_engager': 'int8'})
#### 
print('Train/test split')
# Split training/test data
test_len = int(0.1*len(df_40_M))
df_test = df_40_M.iloc[0:test_len,:]
print('test OK')
df_train = df_40_M.iloc[test_len:len(df_40_M),:]
print('train OK')
print(df_train.columns)
def calc_smooth_mean(df_train, df_test, by, on, m):
    # Compute the global mean
    mean = df_train[on].mean()

    # Compute the number of values and the mean of each group
    agg = df_train.groupby(by)[on].agg(['count', 'mean'])
    counts = agg['count']
    means = agg['mean']

    # Compute the "smoothed" means
    smooth = (counts * means + m * mean) / (counts + m)

    # Replace each value by the according smoothed mean
    return df_train[by].map(smooth), df_test[by].map(smooth)

print('**************************** Target Encoding of train_test data ******************************************')
dtypes_columns = df_train.dtypes
names_columns = df_train.columns
for i, type_ in enumerate(dtypes_columns):
    if type_ == 'object' and names_columns[i]!= 'engaging_user_id' and names_columns[i] != 'tweet_id':
        print(names_columns[i])
        df_train[names_columns[i]], df_test[names_columns[i]] = calc_smooth_mean(df_train, df_test, by=names_columns[i], on='Y', m=10)     
##
Features_train = df_train[['present_domains', 'tweet_type', 'language',
       'present_media', 'engagee_follows_engager', 'hashtags',
       'engaging_user_follower_count', 'engaging_user_following_count',
       'engaged_with_user_follower_count', 'engaged_with_user_following_count',
       'users_int', 'tweets_int',
       'nb_likes', 'nb_reply', 'nb_retweet', 'nb_retweet_C',
       'nb_likes_tweet', 'nb_reply_tweet', 'nb_retweet_tweet', 'nb_retweet_C_tweet',
       'engaging_user_account_creation', 'engaged_with_user_account_creation',
       'engaged_with_user_is_verified', 'engaging_user_is_verified']].values
Features_test= df_test[['present_domains', 'tweet_type', 'language',
       'present_media', 'engagee_follows_engager', 'hashtags',
       'engaging_user_follower_count', 'engaging_user_following_count',
       'engaged_with_user_follower_count', 'engaged_with_user_following_count',
       'users_int', 'tweets_int',
       'nb_likes', 'nb_reply', 'nb_retweet', 'nb_retweet_C',
       'nb_likes_tweet', 'nb_reply_tweet', 'nb_retweet_tweet', 'nb_retweet_C_tweet',
       'engaging_user_account_creation', 'engaged_with_user_account_creation',
       'engaged_with_user_is_verified', 'engaging_user_is_verified']].values
Y_train = df_train['Y']
Y_test = df_test['Y']
##
print('**************************** Scaling the data ******************************************')
##
scaler = MinMaxScaler()
scaler.fit(Features_train)
X_train_scaled = scaler.transform(Features_train)
X_test_scaled = scaler.transform(Features_test)
##
## Save all data (without split)
np.save('/opt/recsys20_challenge/dataset/Data_Reply/XGBoost_Data/Fast_Proto/X_train.npy', X_train_scaled)
np.save('/opt/recsys20_challenge/dataset/Data_Reply/XGBoost_Data/Fast_Proto/Y_train.npy', Y_train)
#####
np.save('/opt/recsys20_challenge/dataset/Data_Reply/XGBoost_Data/Fast_Proto/X_test.npy', X_test_scaled)
np.save('/opt/recsys20_challenge/dataset/Data_Reply/XGBoost_Data/Fast_Proto/Y_test.npy', Y_test)

print('**************************** Data_Saved ******************************************')
## Now creation of eval data for likes
'''
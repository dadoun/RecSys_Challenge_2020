(21849557, 28)
(21849557,)
[12:55:23] INFO: /workspace/src/tree/updater_prune.cc:89: tree pruning end, 1134 extra nodes, 50 pruned nodes, max_depth=10
[0]	validation_0-aucpr:0.25825
Will train until validation_0-aucpr hasn't improved in 3 rounds.
[12:55:49] INFO: /workspace/src/tree/updater_prune.cc:89: tree pruning end, 1160 extra nodes, 94 pruned nodes, max_depth=10
[1]	validation_0-aucpr:0.31712
[12:56:15] INFO: /workspace/src/tree/updater_prune.cc:89: tree pruning end, 1206 extra nodes, 110 pruned nodes, max_depth=10
[2]	validation_0-aucpr:0.32559
[12:56:40] INFO: /workspace/src/tree/updater_prune.cc:89: tree pruning end, 1490 extra nodes, 122 pruned nodes, max_depth=10
[3]	validation_0-aucpr:0.33006
[12:57:06] INFO: /workspace/src/tree/updater_prune.cc:89: tree pruning end, 1328 extra nodes, 150 pruned nodes, max_depth=10
[4]	validation_0-aucpr:0.32937
[12:57:33] INFO: /workspace/src/tree/updater_prune.cc:89: tree pruning end, 1634 extra nodes, 100 pruned nodes, max_depth=10
[5]	validation_0-aucpr:0.33516
[12:57:59] INFO: /workspace/src/tree/updater_prune.cc:89: tree pruning end, 1392 extra nodes, 128 pruned nodes, max_depth=10
[6]	validation_0-aucpr:0.33545
[12:58:24] INFO: /workspace/src/tree/updater_prune.cc:89: tree pruning end, 1508 extra nodes, 102 pruned nodes, max_depth=10
[7]	validation_0-aucpr:0.33812
[12:58:50] INFO: /workspace/src/tree/updater_prune.cc:89: tree pruning end, 1596 extra nodes, 100 pruned nodes, max_depth=10
[8]	validation_0-aucpr:0.33789
[12:59:16] INFO: /workspace/src/tree/updater_prune.cc:89: tree pruning end, 1560 extra nodes, 78 pruned nodes, max_depth=10
[9]	validation_0-aucpr:0.34045
[12:59:43] INFO: /workspace/src/tree/updater_prune.cc:89: tree pruning end, 1486 extra nodes, 104 pruned nodes, max_depth=10
[10]	validation_0-aucpr:0.34120
[13:00:10] INFO: /workspace/src/tree/updater_prune.cc:89: tree pruning end, 1682 extra nodes, 108 pruned nodes, max_depth=10
[11]	validation_0-aucpr:0.34120
[13:00:36] INFO: /workspace/src/tree/updater_prune.cc:89: tree pruning end, 1756 extra nodes, 78 pruned nodes, max_depth=10
[12]	validation_0-aucpr:0.34286
[13:01:02] INFO: /workspace/src/tree/updater_prune.cc:89: tree pruning end, 1658 extra nodes, 56 pruned nodes, max_depth=10
[13]	validation_0-aucpr:0.34459
[13:01:29] INFO: /workspace/src/tree/updater_prune.cc:89: tree pruning end, 1704 extra nodes, 78 pruned nodes, max_depth=10
[14]	validation_0-aucpr:0.34612
**** Training results ****
PRAUC LIKE: PRAUC= 0.35427766147122824
RCE LIKE: RCE= 27.646207429981896
**** Test results ****
PRAUC LIKE: PRAUC= 0.34611874201205356
RCE LIKE: RCE= 27.14874292265351
[['nb_reply' 0.2495197206735611]
 ['engagee_follows_engager' 0.21606270968914032]
 ['tweet_type' 0.15552900731563568]
 ['nb_reply_reader_author' 0.14271600544452667]
 ['users_int' 0.03985350951552391]
 ['nb_reply_author' 0.038174789398908615]
 ['nb_likes' 0.031890060752630234]
 ['authors_int' 0.03169896453619003]
 ['present_domains' 0.01395014300942421]
 ['engaged_with_user_follower_count' 0.011265342123806477]
 ['nb_likes_author' 0.01082132663577795]
 ['nb_likes_reader_author' 0.008585461415350437]
 ['engaging_user_account_creation' 0.005345519166439772]
 ['engaged_with_user_following_count' 0.005072598811239004]
 ['nb_retweet' 0.004713400267064571]
 ['nb_retweet_author' 0.004696208983659744]
 ['language' 0.004607411101460457]
 ['hashtags' 0.004153713118284941]
 ['engaging_user_follower_count' 0.0035569085739552975]
 ['present_media' 0.003257430624216795]
 ['nb_retweet_reader_author' 0.0032214990351349115]
 ['nb_retweet_C_author' 0.0032143318094313145]
 ['engaging_user_following_count' 0.002536969957873225]
 ['engaged_with_user_is_verified' 0.0022282148711383343]
 ['engaged_with_user_account_creation' 0.0015493733808398247]
 ['nb_retweet_C' 0.000929037225432694]
 ['nb_retweet_C_reader_author' 0.000534632068593055]
 ['engaging_user_is_verified' 0.0003157811879646033]]
[13:02:28] ======== Monitor: Learner ========
[13:02:28] Configure: 1e-06s, 1 calls @ 1us

[13:02:28] EvalOneIter: 13.3185s, 15 calls @ 13318520us

[13:02:28] GetGradient: 2.69405s, 15 calls @ 2694054us

[13:02:28] PredictRaw: 0.708453s, 15 calls @ 708453us

[13:02:28] UpdateOneIter: 353.838s, 15 calls @ 353837981us

[13:02:28] ======== Monitor: GBTree ========
[13:02:28] BoostNewTrees: 372.358s, 15 calls @ 372357906us

[13:02:28] CommitModel: 10.864s, 15 calls @ 10864047us

[13:02:28] ======== Monitor:  ========

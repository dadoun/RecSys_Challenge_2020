import pandas as pd
import numpy as np
import random
import os
from sklearn.preprocessing import MinMaxScaler
import time
import csv

## Read training data (Will be splited to tr/test data for local validation & parameters tuning)

print('**************************** READING THE DATA ******************************************')
df_40_M = pd.read_csv('/opt/recsys20_challenge/dataset/Training_20_M_Data.tsv', sep='\x01', encoding='utf-8', header=None,
                      names=["text_ tokens", "hashtags", "tweet_id", "present_media", "present_links", "present_domains",\
                "tweet_type","language", "tweet_timestamp", "engaged_with_user_id", "engaged_with_user_follower_count",\
               "engaged_with_user_following_count", "engaged_with_user_is_verified", "engaged_with_user_account_creation",\
               "engaging_user_id", "engaging_user_follower_count", "engaging_user_following_count", "engaging_user_is_verified",\
               "engaging_user_account_creation", "engagee_follows_engager", "reply_timestamp", "retweet_timestamp", 
                      "retweet_with_comment_timestamp", "like_timestamp"], usecols = ['reply_timestamp'])

df_40_M.reply_timestamp.fillna('NAN', inplace=True)
## Create the target column
## Fill timestamp column
mtx_df = df_40_M['reply_timestamp'].values
list_labels=[]
for i in range(len(mtx_df)):
    if not(mtx_df[i] == 'NAN'):
        list_labels.append(1)
    else:
        list_labels.append(0)
df_40_M['Y']=list_labels
##
df_40_M.drop('reply_timestamp', axis=1, inplace=True)
##
print('NB Y=1: ', np.sum(df_40_M['Y'].values))
print('Len DF: ', len(df_40_M))
##
print('Fill Null Values?')
##
# Split training/test data
test_len = int(0.1*len(df_40_M))
df_test = df_40_M.iloc[0:test_len,:]
print('test OK')
df_train = df_40_M.iloc[test_len:len(df_40_M),:]
print('train OK')

Y_train = df_train['Y']
Y_test = df_test['Y']
##
##
## Save all data (without split)
np.save('/opt/recsys20_challenge/dataset/Data_Reply/XGBoost_Data/Y_train_20_M.npy', Y_train)
#####
np.save('/opt/recsys20_challenge/dataset/Data_Reply/XGBoost_Data/Y_test_20_M.npy', Y_test)

print('**************************** Data_Saved ******************************************')

# 
# Multilingual BERT Feature Extractor for Twitter data
# 
# optional arguments:
#   -h, --help            show this help message and exit
#   -d DATA_PATH, --data_path DATA_PATH
#                         Specify the path for the TSV file to process
#   -o OUTPUT_PATH, --output_path OUTPUT_PATH
#                         Specify the path to which the .npy outputs would be written
#   -t {avg,cls,both}, --embeddings_type {avg,cls,both}
#                         Specify the path to the subtitles folder.
#   -s STARTING_ROW, --starting_row STARTING_ROW
#                         If not processing the entire TSV, set the first row to process
#   -n NUMBER_OF_ROWS, --number_of_rows NUMBER_OF_ROWS
#                         If not processing the entire TSV, set the number of rows to process
#   -gpu VISIBLE_GPUS, --visible_gpus VISIBLE_GPUS
#                         Specify the value for env variable CUDA_VISIBLE_DEVICES (can be comma-separated list), default=0
#   -v, --verbose         Show extra information while loading the data
# 

import os
import time
import torch
import argparse

import numpy as np
import pandas as pd

from tqdm import tqdm
from transformers import BertModel


parser = argparse.ArgumentParser(description='Multilingual BERT Feature Extractor for Twitter data')

parser.add_argument("-d", "--data_path",       type=str, default='/opt/recsys20_challenge/dataset/eval.tsv',
                    help="Specify the path for the TSV file to process")
parser.add_argument("-o", "--output_path",     type=str, default='/opt/recsys20_challenge/bert_embeddings/test/',
                    help="Specify the path to which the .npy outputs would be written")
parser.add_argument("-t", "--embeddings_type", type=str, choices=['avg', 'cls', 'both'], default='both',
                    help="Specify the path to the subtitles folder.")
parser.add_argument("-s", "--starting_row",    type=int, default=0,
                    help="If not processing the entire TSV, set the first row to process") 
parser.add_argument("-n", "--number_of_rows",  type=int, 
                    help="If not processing the entire TSV, set the number of rows to process") 
parser.add_argument("-gpu", "--visible_gpus",  type=str,  default='0',
                    help="Specify the value for env variable CUDA_VISIBLE_DEVICES (can be comma-separated list), default=0") 

args = parser.parse_args()

data_path       = args.data_path
output_path     = args.output_path
visible_gpus    = args.visible_gpus
starting_row    = args.starting_row
number_of_rows  = args.number_of_rows
embeddings_type = args.embeddings_type

os.environ["CUDA_VISIBLE_DEVICES"] = str(visible_gpus)

if not os.path.exists(data_path) :
    print('ERROR: the provided path does not exist.')
    exit()

avg_path = os.path.join(output_path, 'avg/')
cls_path = os.path.join(output_path, 'cls/')

if not os.path.exists(output_path):
    os.mkdir(output_path)
if not os.path.exists(avg_path) and embeddings_type in ['avg', 'both']:
    os.mkdir(avg_path)
if not os.path.exists(cls_path) and embeddings_type in ['cls', 'both']:
    os.mkdir(cls_path)

print(f'Loading mBERT model..')

t = time.time()

model_name = 'bert-base-multilingual-cased'
model      = BertModel.from_pretrained(model_name).to('cuda') # Set model computations on GPU

print(f'BERT model loaded in {time.time() - t:.2f} seconds.')
print(f'Loading data from "{data_path}"..')

t = time.time()

df = pd.read_csv(data_path, usecols=[0], sep='\x01', encoding='utf-8', header=None, 
                 names=["text_tokens"], skiprows=starting_row, nrows=number_of_rows)

print(f'Data loaded in {time.time() - t:.2f} seconds.')
print(f'{len(df)} rows retrieved.')
print(f'First lines (out of {number_of_rows}, starting at {starting_row}):')
print(df.head(3))
print('\nGenerating mBERT embeddings ..')

t = time.time()

with torch.no_grad():
    for i, entry in tqdm(df.iterrows(), total=len(df)):
        input_ids = np.array(entry['text_tokens'].split('\t')).astype(int)
        input_ids = torch.tensor([input_ids]).to('cuda') 
        last_hidden_states = model(input_ids)[0][0]  # Models outputs are now tuples
        output  = last_hidden_states.cpu()
        cls_emb = output[0]
        avg_emb = output[1:-1].mean(axis=0)
        if embeddings_type in ['avg', 'both']:
            np.save(os.path.join(avg_path, f'{starting_row+i}_avg.npy'), avg_emb)
        if embeddings_type in ['cls', 'both']:
            np.save(os.path.join(cls_path, f'{starting_row+i}_cls.npy'), cls_emb)
        
print(f'{i+1} entries processed. Time elapsed: {time.time() - t:.2f} seconds.')
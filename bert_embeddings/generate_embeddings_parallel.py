import os
import math

GPUs_to_use     = [0,1,2,3]
process_per_GPU = 8
start_row = 100
nrows = 1000000
n_processes = (len(GPUs_to_use) * process_per_GPU)
n_per_process = math.ceil(nrows / n_processes)

launched = 0
start_from = start_row
for GPU in GPUs_to_use:
    for process in range(process_per_GPU):
        if launched == n_processes - 1:
            n_per_process = nrows - start_from + start_row
        filename = f'logs/{launched}_{start_from}_{n_per_process}_{GPU}_log.txt'
        os.system(f'python bert_features_extractor.py -s {start_from} -n {n_per_process} -gpu {GPU} > {filename} 2>&1 &')
        start_from += n_per_process
        launched += 1

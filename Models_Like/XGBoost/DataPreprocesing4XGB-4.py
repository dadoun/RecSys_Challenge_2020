import pandas as pd
import numpy as np
import random
import os
from sklearn.preprocessing import MinMaxScaler

print('**************************** READING THE DATA ******************************************')


df_eval = pd.read_csv('/opt/recsys20_challenge/dataset/Data_Likes/XGBoost_Data/df_eval.csv')
#
X_eval= df_eval.to_numpy()
#
np.save('/opt/recsys20_challenge/dataset/Data_Likes/XGBoost_Data/X_eval.npy', X_eval)
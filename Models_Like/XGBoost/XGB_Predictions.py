import joblib
import numpy as np
import pandas as pd
from sklearn.preprocessing import MinMaxScaler

#
X_eval = np.load('/opt/recsys20_challenge/dataset/Data_Likes/XGBoost_Data/X_eval.npy')
#
l_chosen = [12,1,20,0,10,16,5,11,26,3,14,4]#8,18,22]
X_eval = X_eval[:,l_chosen]
#
df_tfidf_likes = pd.read_csv('/home/semantic/recsys20_challenge/code_ismail/predictions/likes_eval_sgdc_tfidf.csv')
##
X_tf_idf_scores_eval = df_tfidf_likes.values[:,3:6]
##
X_eval = np.concatenate([X_eval, X_tf_idf_scores_eval], axis=1)
#
xgb_model = joblib.load("xgb.dat")
#
eval_predictions = xgb_model.predict_proba(X_eval)[:, 1]
#
df_eval = pd.read_csv('/opt/recsys20_challenge/dataset/Data_Likes/XGBoost_Data/uids_tids_p.csv')
#
#
df_eval['Prediction'] = list(eval_predictions)
#
df_eval.rename(columns={'tweet_id':'Tweet_Id',
                          'engaging_user_id':'User_Id'}, 
                 inplace=True)
df_eval = df_eval[['Tweet_Id', 'User_Id', 'Prediction']]
#
df_eval.to_csv('Likes_Predictions.csv', index=None, header=None)
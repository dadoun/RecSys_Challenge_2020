import random
import numpy as np
import pandas as pd
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from matplotlib import pyplot as plt
import itertools
from sklearn.metrics import precision_score, recall_score, roc_auc_score, roc_curve, precision_recall_curve, average_precision_score
from sklearn.metrics import roc_auc_score, confusion_matrix
from sklearn.preprocessing import MinMaxScaler
from xgboost import XGBClassifier
from sklearn.model_selection import StratifiedKFold, KFold
from sklearn.model_selection import StratifiedKFold, RepeatedStratifiedKFold,cross_val_score
from sklearn.metrics import precision_recall_curve, auc, log_loss
import joblib

## Loading the data
X_train = np.load('/opt/recsys20_challenge/dataset/Data_Likes/XGBoost_Data/X_train_20_M.npy')
Y_train = np.load('/opt/recsys20_challenge/dataset/Data_Likes/XGBoost_Data/Y_train_20_M.npy')
## 
X_test = np.load('/opt/recsys20_challenge/dataset/Data_Likes/XGBoost_Data/X_test_20_M.npy')
Y_test = np.load('/opt/recsys20_challenge/dataset/Data_Likes/XGBoost_Data/Y_test_20_M.npy')

l_chosen = [12,1,20,0,10,16,5,11,26,3,14,4]#8,18,22]
X_train = X_train[:,l_chosen]
X_test = X_test[:,l_chosen]

print(X_train.shape)
print(Y_train.shape)
## adding tf_idf_scores

df_tfidf_likes = pd.read_csv('/home/semantic/recsys20_challenge/code_ismail/predictions/likes_20M_sgdc_tfidf.csv')
##
test_len = int(0.1*len(df_tfidf_likes))
df_tfidf_likes_test = df_tfidf_likes.iloc[0:test_len,:]
print('test OK')
df_tfidf_likes_train = df_tfidf_likes.iloc[test_len:len(df_tfidf_likes),:]
print('train OK')
##
X_tf_idf_scores_test = df_tfidf_likes_test.values[:,1:4]
X_tf_idf_scores_train = df_tfidf_likes_train.values[:,1:4]
##
X_train = np.concatenate([X_train, X_tf_idf_scores_train], axis=1)
X_test = np.concatenate([X_test, X_tf_idf_scores_test], axis=1)

print(X_train.shape)
print(X_train.shape)

#lim = int(0.2*len(X_train))

#X_train = X_train[0:lim,:]
#Y_train = Y_train[0:lim]


print('Start Fitting')
## Defining the model

xgb_model = XGBClassifier(objective="binary:logistic", max_depth=10, n_estimators=20, gamma=1, colsample_bytree=0.75,
subsample=0.75, random_state=42, reg_lambda=1.5, verbosity=3, tree_method='approx')
## Fitting tr data
xgb_model.fit(X_train, Y_train, early_stopping_rounds=3, eval_metric="aucpr", eval_set=[(X_test, Y_test)], verbose =True)

joblib.dump(xgb_model, "xgb.dat")

#xgb_model = joblib.load("xgb.dat")
#### Training predictions (to demonstrate overfitting in case)
#train_xgb_predictions = xgb_model.predict(X_train)
train_xgb_probs = xgb_model.predict_proba(X_train)[:, 1]
# Actual class predictions
#xgb_predictions = xgb_model.predict(X_test)
# Probabilities for each class
xgb_probs = xgb_model.predict_proba(X_test)[:, 1]

## Evaluation Metrics
def compute_prauc(pred, gt):
    prec, recall, thresh = precision_recall_curve(gt, pred)
    prauc = auc(recall, prec)
    return prauc

def calculate_ctr(gt):
    positive = len([x for x in gt if x == 1])
    ctr = positive/float(len(gt))
    return ctr

def compute_rce(pred, gt):
    cross_entropy = log_loss(gt, pred)
    data_ctr = calculate_ctr(gt)
    strawman_cross_entropy = log_loss(gt, [data_ctr for _ in range(len(gt))])
    return (1.0 - cross_entropy/strawman_cross_entropy)*100.0 

print('**** Training results ****')
## PR AUC & RCE Metric
prauc = compute_prauc(train_xgb_probs, Y_train)
print('PRAUC LIKE: PRAUC=', prauc)
rce = compute_rce(train_xgb_probs, Y_train)
print('RCE LIKE: RCE=', rce)

print('**** Test results ****')
## PR AUC & RCE Metric
prauc = compute_prauc(xgb_probs, Y_test)
print('PRAUC LIKE: PRAUC=', prauc)
rce = compute_rce(xgb_probs, Y_test)
print('RCE LIKE: RCE=', rce)

l_chosen = ['12','1','20','0','10','16','5','11','26','3','14','4','no_like_softmax', 'like_softmax', 'label']
# Extract feature importances
fi = pd.DataFrame({'feature': l_chosen,
                   'importance': xgb_model.feature_importances_}).\
                    sort_values('importance', ascending = False)

# Display
print(fi.values)

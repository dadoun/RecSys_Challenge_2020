import argparse
from os import path
import joblib
import numpy as np
import pandas as pd

from config import chosen_features, competition_preprocessed, competition_uids_tids, submission_folder, \
    tfidf_prediction_folder

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('what', choices=['like', 'reply', 'retweet', 'retweet_with_comment'], required=True,
                    help='The engagement that you want to predict')

args = parser.parse_args()
what = args.what

# Load data
X_eval = np.load(competition_preprocessed)

l_chosen = chosen_features[what]
X_eval = X_eval[:, l_chosen]

df_tfidf_likes = pd.read_csv(path.join(tfidf_prediction_folder, '%ss_final_sgdc_tfidf_20M.csv.csv' % what))
X_tf_idf_scores_eval = df_tfidf_likes.values[:, 3:6]
X_eval = np.concatenate([X_eval, X_tf_idf_scores_eval], axis=1)

# Load model
xgb_model = joblib.load("xgb.%s.dat" % what)

eval_predictions = xgb_model.predict_proba(X_eval)[:, 1]

df_eval = pd.read_csv(competition_uids_tids)
df_eval['Prediction'] = list(eval_predictions)

df_eval.rename(columns={'tweet_id': 'Tweet_Id',
                        'engaging_user_id': 'User_Id'}, inplace=True)
df_eval = df_eval[['Tweet_Id', 'User_Id', 'Prediction']]

df_eval.to_csv(path.join(submission_folder, 'prediction.%s.csv' % what), index=None, header=None)

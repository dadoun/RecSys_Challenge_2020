from os import path
import argparse

import joblib
import numpy as np
from sklearn.metrics import precision_recall_curve, auc, log_loss
from xgboost import XGBClassifier

from config import chosen_features, preprocessing_folder, folders

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('what', choices=['like', 'reply', 'retweet', 'retweet_with_comment'],
                    help='The engagement that you want to predict')

args = parser.parse_args()
what = args.what

y_folder = folders[what]

# Loading the data
X_train = np.load(path.join(preprocessing_folder, 'X_train_20_M.npy'))
Y_train = np.load(path.join(y_folder, 'Y_train.npy'))

X_test = np.load(path.join(preprocessing_folder, 'X_test_20_M.npy'))
Y_test = np.load(path.join(y_folder, 'Y_test.npy'))

print('Data Downloaded')
l_chosen = chosen_features[what]
X_train = X_train[:, l_chosen]
X_test = X_test[:, l_chosen]

print(X_train.shape)
print(Y_train.shape)

# Defining the model
xgb_model = XGBClassifier(objective="binary:logistic", max_depth=10,
                          n_estimators=15, gamma=1, colsample_bytree=0.75,
                          subsample=0.75, random_state=42, reg_lambda=1.5, verbosity=3, tree_method='approx')

# Fitting tr data
xgb_model.fit(X_train, Y_train, early_stopping_rounds=3, eval_metric="aucpr", eval_set=[(X_test, Y_test)], verbose=True)

joblib.dump(xgb_model, "xgb.%s.dat" % what)

# Training predictions (to demonstrate overfitting in case)
train_xgb_probs = xgb_model.predict_proba(X_train)[:, 1]
# Probabilities for each class
xgb_probs = xgb_model.predict_proba(X_test)[:, 1]


# Evaluation Metrics
def compute_prauc(pred, gt):
    prec, recall, thresh = precision_recall_curve(gt, pred)
    prauc = auc(recall, prec)
    return prauc


def calculate_ctr(gt):
    positive = len([x for x in gt if x == 1])
    ctr = positive / float(len(gt))
    return ctr


def compute_rce(pred, gt):
    cross_entropy = log_loss(gt, pred)
    data_ctr = calculate_ctr(gt)
    strawman_cross_entropy = log_loss(gt, [data_ctr for _ in range(len(gt))])
    return (1.0 - cross_entropy / strawman_cross_entropy) * 100.0


print('**** Training results ****')
## PR AUC & RCE Metric
prauc = compute_prauc(train_xgb_probs, Y_train)
print('PRAUC LIKE: PRAUC=', prauc)
rce = compute_rce(train_xgb_probs, Y_train)
print('RCE LIKE: RCE=', rce)

print('**** Test results ****')
## PR AUC & RCE Metric
prauc = compute_prauc(xgb_probs, Y_test)
print('PRAUC LIKE: PRAUC=', prauc)
rce = compute_rce(xgb_probs, Y_test)
print('RCE LIKE: RCE=', rce)

# l = ['present_domains', 'tweet_type', 'language',
#      'present_media', 'engagee_follows_engager', 'hashtags',
#      'engaging_user_follower_count', 'engaging_user_following_count',
#      'engaged_with_user_follower_count', 'engaged_with_user_following_count',
#      'users_int', 'authors_int',
#      'nb_likes', 'nb_reply', 'nb_retweet', 'nb_retweet_C',
#      'nb_likes_author', 'nb_reply_author', 'nb_retweet_author', 'nb_retweet_C_author',
#      'nb_likes_reader_author', 'nb_reply_reader_author', 'nb_retweet_reader_author', 'nb_retweet_C_reader_author',
#      'engaging_user_account_creation', 'engaged_with_user_account_creation',
#      'engaged_with_user_is_verified', 'engaging_user_is_verified']
# # Extract feature importances
# fi = pd.DataFrame({'feature': l,
#                    'importance': xgb_model.feature_importances_}). \
#     sort_values('importance', ascending=False)
#
# # Display
# print(fi.values)

import argparse
from os import path

import numpy as np
import pandas as pd

from config import base_folder, folders

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('what', choices=['like', 'reply', 'retweet', 'retweet_with_comment'], required=True,
                    help='The engagement that you want to predict')

args = parser.parse_args()
what = args.what

# INPUT FOLDER
output_path = folders[what]

print('**************************** READING THE DATA ******************************************')
field = what + '_timestamp'

df = pd.read_csv(path.join(base_folder, 'training.tsv'), sep='\x01', encoding='utf-8', header=None,
                 names=["text_ tokens", "hashtags", "tweet_id", "present_media", "present_links",
                        "present_domains", "tweet_type", "language", "tweet_timestamp", "engaged_with_user_id",
                        "engaged_with_user_follower_count", "engaged_with_user_following_count",
                        "engaged_with_user_is_verified", "engaged_with_user_account_creation",
                        "engaging_user_id", "engaging_user_follower_count", "engaging_user_following_count",
                        "engaging_user_is_verified", "engaging_user_account_creation",
                        "engagee_follows_engager", "reply_timestamp", "retweet_timestamp",
                        "retweet_with_comment_timestamp", "like_timestamp"],
                 usecols=[field])

df[field].fillna('NAN', inplace=True)

# Create the target column
# Fill timestamp column
mtx_df = df[field].values
list_labels = []
for i in range(len(mtx_df)):
    if not (mtx_df[i] == 'NAN'):
        list_labels.append(1)
    else:
        list_labels.append(0)
df['Y'] = list_labels
df.drop(field, axis=1, inplace=True)
print('NB Y=1: ', np.sum(df['Y'].values))
print('Len DF: ', len(df))

print('Fill Null Values?')
# Split training/test data
test_len = int(0.1 * len(df))
df_test = df.iloc[0:test_len, :]
print('test OK')
df_train = df.iloc[test_len:len(df), :]
print('train OK')

Y_train = df_train['Y']
Y_test = df_test['Y']

# Save all data (without split)
np.save(path.join(output_path, 'Y_train.npy'), Y_train)
np.save(path.join(output_path, 'Y_test.npy'), Y_test)

print('**************************** Data_Saved ******************************************')

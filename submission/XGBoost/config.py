from os import path

submission_folder = '../submission'

base_folder = '/opt/recsys20_challenge/dataset'  # folder where your .tsv files are
preprocessing_folder = path.join(base_folder, 'Data_Likes/XGBoost_Data/')
tfidf_prediction_folder = './tfidf_prediction'

competition_preprocessed = path.join(preprocessing_folder, 'X_competition_test.npy')
competition_uids_tids = path.join(preprocessing_folder, 'uids_tids_p_competition_test.csv')
folders = {
    'like': path.join(base_folder, 'Data_Likes/XGBoost_Data/'),
    'reply': path.join(base_folder, 'Data_Reply/XGBoost_Data/'),
    'retweet': path.join(base_folder, 'Data_Retweet/XGBoost_Data/'),
    'retweet_with_comment': path.join(base_folder, 'Data_Retweet_C/XGBoost_Data/')
}

chosen_features = {
    'like': [12, 1, 20, 0, 10, 16, 5, 11, 26, 3, 14, 4],
    'reply': [13, 4, 1, 21, 10, 17, 12, 11, 3, 8, 16, 20, 24, 9, 14, 18, 2, 5, 6, 3, 22, 23],
    'retweet': [14, 22, 10, 18, 26, 11, 20, 12, 6, 1, 2, 8, 3, 19, 7],
    'retweet_with_comment': [15, 14, 23, 10, 19, 11, 4, 6, 12, 18, 20, 1, 16, 2, 8]
}

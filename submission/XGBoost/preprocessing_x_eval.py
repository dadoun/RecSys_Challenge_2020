from os import path
import pandas as pd
import numpy as np

from config import base_folder, preprocessing_folder

print('**************************** READING THE DATA - Train, Test ******************************************')

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('what', choices=['competition_test', 'eval'], required=True,
                    help='Set you want to preprocess')

args = parser.parse_args()
what = args.what

training_file = path.join(base_folder, 'train_20_M.csv')
test_file = path.join(base_folder, 'test_20_M.csv')
evaluation_file = path.join(base_folder, '%s.tsv' % what)

df_train_target_encode = pd.read_csv(training_file,
                                     usecols=['present_domains', 'tweet_type', 'language',
                                              'present_media', 'hashtags', 'Y'])

print('train, test target encode finished')

df_train_fea = pd.read_csv(training_file,
                           usecols=['engaging_user_id', 'tweet_id', 'engaged_with_user_id',
                                    'Y', 'reply_Y', 'retweet_Y', 'retweet_C_Y'])

df_test_fea = pd.read_csv(test_file,
                          usecols=['engaging_user_id', 'tweet_id', 'engaged_with_user_id',
                                   'Y', 'reply_Y', 'retweet_Y', 'retweet_C_Y'])

print('train, test features computation finished')

df_all_fea = pd.concat([df_train_fea, df_test_fea])

del df_train_fea
del df_test_fea

print('**************************** Reading DF eval ******************************************')

df_eval = pd.read_csv(evaluation_file, sep='\x01', encoding='utf-8', header=None,
                      names=["text_ tokens", "hashtags", "tweet_id", "present_media", "present_links",
                             "present_domains", "tweet_type", "language", "tweet_timestamp",
                             "engaged_with_user_id", "engaged_with_user_follower_count",
                             "engaged_with_user_following_count", "engaged_with_user_is_verified",
                             "engaged_with_user_account_creation", "engaging_user_id",
                             "engaging_user_follower_count", "engaging_user_following_count",
                             "engaging_user_is_verified", "engaging_user_account_creation",
                             "engagee_follows_engager"],
                      usecols=['engaging_user_id', 'tweet_id', 'engaged_with_user_id', 'present_domains',
                               'tweet_type', 'language', 'present_media', 'engagee_follows_engager', 'hashtags',
                               'engaging_user_follower_count', 'engaging_user_following_count',
                               'engaged_with_user_follower_count', 'engaged_with_user_following_count',
                               'engaged_with_user_is_verified', 'engaging_user_is_verified',
                               'engaging_user_account_creation', 'engaged_with_user_account_creation'])


# Retrieve users & tweets that are in initial data set
def tweet_users_int(df_all, df_eval):
    df_users_id = df_all.engaging_user_id.value_counts()
    dict_tr_users_nb_int = dict(zip(list(df_users_id.index), list(df_users_id)))
    ##
    df_authors_id = df_all.engaged_with_user_id.value_counts()
    dict_tr_authors_nb_int = dict(zip(list(df_authors_id.index), list(df_authors_id)))

    df_users_id = df_eval.engaging_user_id.value_counts()
    dict_users_nb_int = dict(zip(list(df_users_id.index), list(df_users_id)))

    list_nb_int_users = list()
    mtx = df_eval['engaging_user_id'].values
    for i in range(len(df_eval)):
        if mtx[i] in dict_tr_users_nb_int:
            list_nb_int_users.append(dict_users_nb_int[mtx[i]] + dict_tr_users_nb_int[mtx[i]])
        else:
            list_nb_int_users.append(dict_users_nb_int[mtx[i]])
    df_eval['users_int'] = list_nb_int_users

    df_authors_id = df_eval.engaged_with_user_id.value_counts()
    dict_authors_nb_int = dict(zip(list(df_authors_id.index), list(df_authors_id)))

    list_nb_int_authors = list()
    mtx = df_eval['engaged_with_user_id'].values
    for i in range(len(df_eval)):
        if mtx[i] in dict_tr_authors_nb_int:
            list_nb_int_authors.append(dict_authors_nb_int[mtx[i]] + dict_tr_authors_nb_int[mtx[i]])
        else:
            list_nb_int_authors.append(dict_authors_nb_int[mtx[i]])
    df_eval['authors_int'] = list_nb_int_authors
    return df_eval


def nb_eng_reader_author(engagement, column, df, df_eval):
    df_Y_1 = df[df[column] == 1]
    df_likes = df_Y_1.groupby(['engaging_user_id', 'engaged_with_user_id']).size().reset_index(
        name='nb_likes_reader_author')
    df_likes['nb_likes_reader_author'] = df_likes['nb_likes_reader_author']
    dict_nb_likes = dict()
    mtx_1 = df_likes.values
    for item in mtx_1:
        dict_nb_likes[item[0] + '_' + item[1]] = item[2]
    mtx = df_eval[['engaging_user_id', 'engaged_with_user_id']].values
    list_nb_likes = list()
    for elt in mtx:
        reader = elt[0]
        author = elt[1]
        if reader + '_' + author in dict_nb_likes:
            list_nb_likes.append(dict_nb_likes[reader + '_' + author])
        else:
            list_nb_likes.append(0)
    df_eval['nb_' + engagement + '_' + 'reader_author'] = list_nb_likes
    return df_eval


print('Compute nb likes author reader')
df_eval = nb_eng_reader_author('likes', 'Y', df_all_fea, df_eval)
df_eval = nb_eng_reader_author('reply', 'reply_Y', df_all_fea, df_eval)
df_eval = nb_eng_reader_author('retweet', 'retweet_Y', df_all_fea, df_eval)
df_eval = nb_eng_reader_author('retweet_C', 'retweet_C_Y', df_all_fea, df_eval)

# df_eval = nb_likes_reader_author(df=df_all_fea, df_eval=df_eval)

print('Tweet-Users int computation')
df_eval = tweet_users_int(df_all_fea, df_eval)

print('change type')
df_eval = df_eval.astype({'engaging_user_is_verified': 'int8'})
df_eval = df_eval.astype({'engaged_with_user_is_verified': 'int8'})
df_eval = df_eval.astype({'engagee_follows_engager': 'int8'})


def nb_eng_eval(engagement, column, df, df_eval):
    df_Y_1 = df[df[column] == 1].engaging_user_id.value_counts()
    dict_nb_likes = dict(zip(list(df_Y_1.index), list(df_Y_1)))
    ### For users
    list_nb_likes = list()
    mtx = df_eval['engaging_user_id'].values
    for i in range(len(df_eval)):
        if mtx[i] in dict_nb_likes:
            list_nb_likes.append(dict_nb_likes[mtx[i]])
        else:
            list_nb_likes.append(0)
    df_eval['nb_' + engagement] = list_nb_likes
    return df_eval


def nb_eng_eval_author(engagement, column, df, df_eval):
    df_Y_1 = df[df[column] == 1].engaged_with_user_id.value_counts()
    dict_nb_likes = dict(zip(list(df_Y_1.index), list(df_Y_1)))
    # For users
    list_nb_likes = list()
    mtx = df_eval['engaged_with_user_id'].values
    for i in range(len(df_eval)):
        if mtx[i] in dict_nb_likes:
            list_nb_likes.append(dict_nb_likes[mtx[i]])
        else:
            list_nb_likes.append(0)
    df_eval['nb_' + engagement + '_author'] = list_nb_likes
    return df_eval


print('Adding nb eng users')
df_eval = nb_eng_eval('likes', 'Y', df_all_fea, df_eval)
df_eval = nb_eng_eval('reply', 'reply_Y', df_all_fea, df_eval)
df_eval = nb_eng_eval('retweet', 'retweet_Y', df_all_fea, df_eval)
df_eval = nb_eng_eval('retweet_C', 'retweet_C_Y', df_all_fea, df_eval)

print('Adding nb eng authors')
# Compute nb_interaction per type of interaction for tweets
df_eval = nb_eng_eval_author('likes', 'Y', df_all_fea, df_eval)
df_eval = nb_eng_eval_author('reply', 'reply_Y', df_all_fea, df_eval)
df_eval = nb_eng_eval_author('retweet', 'retweet_Y', df_all_fea, df_eval)
df_eval = nb_eng_eval_author('retweet_C', 'retweet_C_Y', df_all_fea, df_eval)

print(df_eval.columns)
df_eval.fillna('NAN', inplace=True)


def calc_smooth_mean(df_train, df_test, by, on, m):
    # Compute the global mean
    mean = df_train[on].mean()

    # Compute the number of values and the mean of each group
    agg = df_train.groupby(by)[on].agg(['count', 'mean'])
    counts = agg['count']
    means = agg['mean']

    # Compute the "smoothed" means
    smooth = (counts * means + m * mean) / (counts + m)

    # Replace each value by the according smoothed mean
    return df_train[by].map(smooth), df_test[by].map(smooth)


# Target encode cat features
dtypes_columns = df_train_target_encode.dtypes
names_columns = df_train_target_encode.columns
for i, type_ in enumerate(dtypes_columns):
    if type_ == 'object' and names_columns[i] != 'engaging_user_id' and names_columns[i] != 'engaged_with_user_id' and \
            names_columns[i] != 'tweet_id':
        print(names_columns[i])
        _, df_eval[names_columns[i]] = calc_smooth_mean(df_train_target_encode, df_eval,
                                                        by=names_columns[i], on='Y', m=10)
print('Target Encoding ended')

del df_all_fea

df_eval[['present_domains', 'tweet_type', 'language',
         'present_media', 'engagee_follows_engager', 'hashtags',
         'engaging_user_follower_count', 'engaging_user_following_count',
         'engaged_with_user_follower_count', 'engaged_with_user_following_count',
         'users_int', 'authors_int',
         'nb_likes', 'nb_reply', 'nb_retweet', 'nb_retweet_C',
         'nb_likes_author', 'nb_reply_author', 'nb_retweet_author', 'nb_retweet_C_author',
         'nb_likes_reader_author', 'nb_reply_reader_author', 'nb_retweet_reader_author',
         'nb_retweet_C_reader_author',
         'engaging_user_account_creation', 'engaged_with_user_account_creation',
         'engaged_with_user_is_verified', 'engaging_user_is_verified']].to_csv(
    path.join(preprocessing_folder, 'df_%s.csv' % what), index=None)
df_eval[['engaging_user_id', 'tweet_id']].to_csv(
    path.join(preprocessing_folder, 'uids_tids_p_%s.csv' % what), index=None)

X_eval = df_eval.to_numpy()
np.save(path.join(preprocessing_folder, 'X_%s.npy' % what), X_eval)

from os import path
import numpy as np
import pandas as pd

from config import base_folder, preprocessing_folder

print('**************************** READING THE DATA ******************************************')

training_file = path.join(base_folder, 'train_40_M.csv')
test_file = path.join(base_folder, 'test_40_M.csv')

df_train = pd.read_csv(training_file,
                       usecols=['present_domains', 'tweet_type', 'language',
                                'present_media', 'engagee_follows_engager', 'hashtags',
                                'engaging_user_follower_count', 'engaging_user_following_count',
                                'engaged_with_user_follower_count', 'engaged_with_user_following_count',
                                'users_int', 'authors_int',
                                'nb_likes', 'nb_reply', 'nb_retweet', 'nb_retweet_C',
                                'nb_likes_author', 'nb_reply_author', 'nb_retweet_author',
                                'nb_retweet_C_author',
                                'nb_likes_reader_author', 'nb_reply_reader_author',
                                'nb_retweet_reader_author', 'nb_retweet_C_reader_author',
                                'engaging_user_account_creation', 'engaged_with_user_account_creation',
                                'engaged_with_user_is_verified', 'engaging_user_is_verified', 'Y'])
df_test = pd.read_csv(test_file,
                      usecols=['present_domains', 'tweet_type', 'language',
                               'present_media', 'engagee_follows_engager', 'hashtags',
                               'engaging_user_follower_count', 'engaging_user_following_count',
                               'engaged_with_user_follower_count', 'engaged_with_user_following_count',
                               'users_int', 'authors_int',
                               'nb_likes', 'nb_reply', 'nb_retweet', 'nb_retweet_C',
                               'nb_likes_author', 'nb_reply_author', 'nb_retweet_author', 'nb_retweet_C_author',
                               'nb_likes_reader_author', 'nb_reply_reader_author', 'nb_retweet_reader_author',
                               'nb_retweet_C_reader_author',
                               'engaging_user_account_creation', 'engaged_with_user_account_creation',
                               'engaged_with_user_is_verified', 'engaging_user_is_verified', 'Y'])

print(df_train.shape)

print(df_test.shape)

print('Fill Null Values')

df_train.fillna('NAN', inplace=True)
df_test.fillna('NAN', inplace=True)

print('Data type change')


def bool2int(df, column):
    ####
    mtx_bool = df[column].values
    list_bool = []
    for i in range(len(mtx_bool)):
        if mtx_bool[i]:
            list_bool.append(1)
        else:
            list_bool.append(0)
    df[column] = list_bool
    return df


df_train = bool2int(df_train, 'engagee_follows_engager')
df_train = bool2int(df_train, 'engaging_user_is_verified')
df_train = bool2int(df_train, 'engaged_with_user_is_verified')

print('**************************** Target Encoding of train_test data ******************************************')


def calc_smooth_mean(df_train, df_test, by, on, m):
    # Compute the global mean
    mean = df_train[on].mean()
    print('mean OK')
    # Compute the number of values and the mean of each group
    agg = df_train.groupby(by)[on].agg(['count', 'mean'])
    counts = agg['count']
    print('counts OK')
    means = agg['mean']
    print('means OK')
    # Compute the "smoothed" means
    smooth = (counts * means + m * mean) / (counts + m)
    print('SMOOTH OK')
    # Replace each value by the according smoothed mean
    return df_train[by].map(smooth), df_test[by].map(smooth)


def compute_encode_target(df_train, df_test):
    print('Target Encoding')
    dtypes_columns = df_train.dtypes
    names_columns = df_train.columns
    for i, type_ in enumerate(dtypes_columns):
        if type_ != 'object':
            continue
        print(names_columns[i])
        df_train[names_columns[i]], df_test[names_columns[i]] = calc_smooth_mean(
            df_train, df_test, by=names_columns[i], on='Y', m=10)

    return df_train, df_test


df_train, df_test = compute_encode_target(df_train, df_test)

print('Conversion to numpy')
X_train = df_train[['present_domains', 'tweet_type', 'language',
                    'present_media', 'engagee_follows_engager', 'hashtags',
                    'engaging_user_follower_count', 'engaging_user_following_count',
                    'engaged_with_user_follower_count', 'engaged_with_user_following_count',
                    'users_int', 'authors_int',
                    'nb_likes', 'nb_reply', 'nb_retweet', 'nb_retweet_C',
                    'nb_likes_author', 'nb_reply_author', 'nb_retweet_author', 'nb_retweet_C_author',
                    'nb_likes_reader_author', 'nb_reply_reader_author', 'nb_retweet_reader_author',
                    'nb_retweet_C_reader_author',
                    'engaging_user_account_creation', 'engaged_with_user_account_creation',
                    'engaged_with_user_is_verified', 'engaging_user_is_verified']].to_numpy()

print('OK for numpy')
# Save all data (without split)
np.save(path.join(preprocessing_folder, 'X_train_20_M.npy'), X_train)
np.save(path.join(preprocessing_folder, 'Y_train.npy'), df_train['Y'])

df_test = df_test.astype({'engagee_follows_engager': 'int8'})
df_test = df_test.astype({'engaging_user_is_verified': 'int8'})
df_test = df_test.astype({'engaged_with_user_is_verified': 'int8'})
X_test = df_test[['present_domains', 'tweet_type', 'language',
                  'present_media', 'engagee_follows_engager', 'hashtags',
                  'engaging_user_follower_count', 'engaging_user_following_count',
                  'engaged_with_user_follower_count', 'engaged_with_user_following_count',
                  'users_int', 'authors_int',
                  'nb_likes', 'nb_reply', 'nb_retweet', 'nb_retweet_C',
                  'nb_likes_author', 'nb_reply_author', 'nb_retweet_author', 'nb_retweet_C_author',
                  'nb_likes_reader_author', 'nb_reply_reader_author', 'nb_retweet_reader_author',
                  'nb_retweet_C_reader_author',
                  'engaging_user_account_creation', 'engaged_with_user_account_creation',
                  'engaged_with_user_is_verified', 'engaging_user_is_verified']].to_numpy()
np.save(path.join(preprocessing_folder, 'X_test_20_M.npy'), X_test)
np.save(path.join(preprocessing_folder, 'Y_test.npy'), df_test['Y'])

print('**************************** Data_Saved ******************************************')

from os import path
import numpy as np
import pandas as pd

from config import base_folder

print('**************************** READING THE DATA ******************************************')
training_file = path.join(base_folder, 'training.tsv')

df_all = pd.read_csv(training_file, sep='\x01', encoding='utf-8', header=None,
                     names=["text_ tokens", "hashtags", "tweet_id", "present_media", "present_links",
                            "present_domains", "tweet_type", "language", "tweet_timestamp", "engaged_with_user_id",
                            "engaged_with_user_follower_count", "engaged_with_user_following_count",
                            "engaged_with_user_is_verified", "engaged_with_user_account_creation",
                            "engaging_user_id", "engaging_user_follower_count", "engaging_user_following_count",
                            "engaging_user_is_verified", "engaging_user_account_creation", "engagee_follows_engager",
                            "reply_timestamp", "retweet_timestamp",
                            "retweet_with_comment_timestamp", "like_timestamp"],
                     usecols=['engaging_user_id', 'tweet_id', 'engaged_with_user_id', 'present_domains', 'tweet_type',
                              'language', 'present_media', 'engagee_follows_engager', 'hashtags',
                              'engaging_user_follower_count', 'engaging_user_following_count',
                              'engaged_with_user_follower_count', 'engaged_with_user_following_count',
                              'engaged_with_user_is_verified', 'engaging_user_is_verified',
                              'engaging_user_account_creation', 'engaged_with_user_account_creation',
                              'reply_timestamp', 'retweet_timestamp', 'retweet_with_comment_timestamp',
                              'like_timestamp'])


def author_reader_int(df):
    df_users_id = df.engaging_user_id.value_counts()
    dict_users_nb_int = dict(zip(list(df_users_id.index), list(df_users_id)))
    # For users
    list_nb_int_users = list()
    mtx = df['engaging_user_id'].values
    for i in range(len(df)):
        list_nb_int_users.append(dict_users_nb_int[mtx[i]])
    df['users_int'] = list_nb_int_users
    # For Tweets
    df_authors_id = df.engaged_with_user_id.value_counts()
    dict_authors_nb_int = dict(zip(list(df_authors_id.index), list(df_authors_id)))

    list_nb_int_authors = list()
    mtx = df['engaged_with_user_id'].values
    for i in range(len(df)):
        list_nb_int_authors.append(dict_authors_nb_int[mtx[i]])
    df['authors_int'] = list_nb_int_authors
    return df


def boolean_replacement(df, column, new_column):
    df[column].fillna('NAN', inplace=True)
    # Create the target column
    # Fill timestamp column
    mtx_df = df[column].values
    list_labels = []
    for i in range(len(mtx_df)):
        if not (mtx_df[i] == 'NAN'):
            list_labels.append(1)
        else:
            list_labels.append(0)
    df[new_column] = list_labels
    df.drop(column, axis=1, inplace=True)
    return df


def nb_eng_reader(engagement, column, df):
    df_Y_1 = df[df[column] == 1].engaging_user_id.value_counts() - 1
    dict_nb_likes = dict(zip(list(df_Y_1.index), list(df_Y_1)))
    # For users
    list_nb_likes = list()
    mtx = df['engaging_user_id'].values
    for i in range(len(df)):
        if mtx[i] in dict_nb_likes:
            list_nb_likes.append(dict_nb_likes[mtx[i]])
        else:
            list_nb_likes.append(0)
    df['nb_' + engagement] = list_nb_likes
    return df


def nb_eng_author(engagement, column, df):
    df_Y_1 = df[df[column] == 1].engaged_with_user_id.value_counts() - 1
    dict_nb_likes = dict(zip(list(df_Y_1.index), list(df_Y_1)))
    # For users
    list_nb_likes = list()
    mtx = df['engaged_with_user_id'].values
    for i in range(len(df)):
        if mtx[i] in dict_nb_likes:
            list_nb_likes.append(dict_nb_likes[mtx[i]])
        else:
            list_nb_likes.append(0)
    df['nb_' + engagement + '_author'] = list_nb_likes
    return df


def nb_eng_reader_author(engagement, column, df):
    df_Y_1 = df[df[column] == 1]
    df_likes = df_Y_1.groupby(['engaging_user_id', 'engaged_with_user_id']) \
        .size().reset_index(name='nb_likes_reader_author')
    df_likes['nb_likes_reader_author'] = df_likes['nb_likes_reader_author'] - 1
    dict_nb_likes = dict()
    mtx_1 = df_likes.values
    for item in mtx_1:
        dict_nb_likes[item[0] + '_' + item[1]] = item[2]
    mtx = df[['engaging_user_id', 'engaged_with_user_id']].values
    list_nb_likes = list()
    for elt in mtx:
        reader = elt[0]
        author = elt[1]
        if reader + '_' + author in dict_nb_likes:
            list_nb_likes.append(dict_nb_likes[reader + '_' + author])
        else:
            list_nb_likes.append(0)
    df['nb_' + engagement + '_' + 'reader_author'] = list_nb_likes
    return df


print('**************** Boolean Replacement *******************')
df_all = boolean_replacement(df_all, 'like_timestamp', 'Y')
df_all = boolean_replacement(df_all, 'reply_timestamp', 'reply_Y')
df_all = boolean_replacement(df_all, 'retweet_timestamp', 'retweet_Y')
df_all = boolean_replacement(df_all, 'retweet_with_comment_timestamp', 'retweet_C_Y')
print('**************************** ADDING nb eng reader author columns ******************************************')
## Add nb likes reader author
df_all = nb_eng_reader_author('likes', 'Y', df_all)
df_all = nb_eng_reader_author('reply', 'reply_Y', df_all)
df_all = nb_eng_reader_author('retweet', 'retweet_Y', df_all)
df_all = nb_eng_reader_author('retweet_C', 'retweet_C_Y', df_all)
print('**************************** ADDING (user,tweet_int) columns ******************************************')
## Add two columns users_int, items_int
df_all = author_reader_int(df_all)
##########
print('**************** Interactions per Engagement *******************')
# Compute nb_interaction per type of interaction
df_all = nb_eng_reader('likes', 'Y', df_all)
df_all = nb_eng_reader('reply', 'reply_Y', df_all)
df_all = nb_eng_reader('retweet', 'retweet_Y', df_all)
df_all = nb_eng_reader('retweet_C', 'retweet_C_Y', df_all)
# # Compute nb_interaction per type of interaction for tweets
df_all = nb_eng_author('likes', 'Y', df_all)
df_all = nb_eng_author('reply', 'reply_Y', df_all)
df_all = nb_eng_author('retweet', 'retweet_Y', df_all)
df_all = nb_eng_author('retweet_C', 'retweet_C_Y', df_all)

# What is the size of our dataset?
print('NB Y=1: ', np.sum(df_all['Y'].values))
print('Len DF: ', len(df_all))

print('Train/test split')
# Split training/test data
test_len = int(0.1 * len(df_all))
df_test = df_all.iloc[0:test_len, :]
print('test OK')
df_train = df_all.iloc[test_len:len(df_all), :]
print('train OK')
print(df_train.columns)

df_train.to_csv(path.join(base_folder, 'train_40_M.csv'), index=None)
df_test.to_csv(path.join(base_folder, 'test_40_M.csv'), index=None)
